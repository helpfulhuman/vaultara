<div id="circleContainer">

	<a class="noDecoration" href="send-images.php">
		<div class="circleChoice">
			<p class="circleChoiceHeader">
				DICOM
			</p>
			<img src="images/icon-send.png">
			<div class="circleChoiceDesc">
				Send using DICOM
			</div>
		</div>
	</a>

	<a class="noDecoration" href="send-data.php">
		<div class="circleChoice">
			<p class="circleChoiceHeader">
				Files
			</p>
			<img src="images/icon-receive.png">
			<div class="circleChoiceDesc">
				Send using uploaded files
			</div>
		</div>
	</a>

</div>