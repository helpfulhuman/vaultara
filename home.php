<!DOCTYPE html>
<html class="home-background">
<head>
	<title>Home</title>
	<?php
		include 'partials/head.php';
	?>


</head>
<body>
<div id="container">
	<?php
		include 'partials/navbar.php';
	?>

	<div id="companyBanner" class="home-banner">

		<img id="home-logo-image" src="images/glowgo.png">
		<!-- <p id="vaultaraLogo">vaultara
		</p> -->
	</div>

	<?php
		include 'homeMainNav.php';
	?>

	<?php
		include 'footer.php';
	?>
</div>
</body>
</html>
