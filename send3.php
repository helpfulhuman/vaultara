<?php
		include 'partials/send3_data.php';
?>

<!DOCTYPE html>
<html class="send-background">
<head>

	<title>Send</title>

	<?php
		include 'partials/head.php';
	?>


</head>
<body>
<div id="container">
	<?php
		include 'partials/navbar.php';
	?>

	<div class="banner-background">
		<div id="companyBanner" class="send-banner">

			<img id="send-logo-image" src="images/glowgo.png">

		</div>
	</div>

	<div class="send-container">
		<div class="send-header">
			<h2 class="header-text" id="send-head"><img id="send-icon" src="images/icon-send.png"></img>Send Images</h2>
		</div>
		<div class="send-subHeader">

			<div class="step1-item">
				<span class="step-text">STEP</span>
				<div class="step-circle">2</div>
				<img id="step-checked-img" src="images/step-check.png">
			</div>

			<span class="step-text">STEP</span>
			<div class="step-circle">3</div>
			<span class="send-filter-text">DETAILS</span>
			<span class="instruction-text">Review the <b class="bolded">To, Retention,</b> and <b class="bolded">Permissions.</b></span>
			<span class="instruction-text">Then check the digital signature box to proceed.</span>

		</div>

		<div class="form-container send-3-container">
			<form class="data-form form-3-content" id="form_3" action="send-confirm.php">
				<div class="upper-form">
					<div class="form-section">
						<label class="form-label send" for="recipient-field" id="send-recipient-to">To</label>
	  				<input class="form-input" type="email" name="" id="recipient-field" value="" required><br>
	  			</div>
	  			<div class="form-section">
	  				<label class="form-label receive">From</label>
	  				<span class="sender"><?php echo $client['user_email']; ?></span><br>
	  			</div>
	  			<div class="form-section">
	  				<label class="form-label" for="availability-field">Availability</label>
	  				<input class="form-input" type="text" name="" id="availability-field" value="" required><br>
	  			</div>

	  			<div class="form-section permissions">
	  				<label class="form-label perm-label">Permissions</label>

	  				<div class="permissions-box">

	  					<div class="perm-left">
	  						<div class="header-section">Online</div>

	  						<!-- <input type="checkbox" name="online" id="online">
	  						<label for="online">VIEW</label>	 -->
	  						<div class="check-container">
		  						<input class="check-input" type="checkbox" name="selected" value="" id="online" />
									<div class="checkbox" for="online"></div>
									<span>VIEW</span>
								</div>
	  					</div>

	  					<div class="perm-right">
	  						<div class="header-section">Download</div>
	  						<div class="check-container">
		  						<input class="check-input" type="checkbox" name="selected" value="" id="viewer" />
									<div class="checkbox" for="viewer"></div>
									<span class="viewer-text">VIEWER</span>

		  						<input class="check-input" type="checkbox" name="selected" value="" id="images" />
									<div class="checkbox" for="images"></div>
									<span>IMAGES</span>
								</div>
	  					</div>

	  				</div>

	  			</div>

				</div>

				<div class="lower-form">

					<?php foreach($items as $itemIndex => $item): ?>
					<div class="medical-file">
						<div class="file-section one">
							<span class="med-text"><?php echo $item['client_id']; ?></span>
						</div>
						<div class="file-section two">
							<span class="med-text">MR89</span>
						</div>
						<div class="file-section three">
							<span class="med-text">MG</span>
							<a href="#" ><div class="expand-button"><span>+</span></div></a>
						</div>
					</div>
					<?php endforeach; ?>

					<div class="text-section">
						<p>Synth Odd Future Etsy lo-fi, kitsch gentrify next level sustainable banh mi health goth. Lo-fi cred mustache, Thundercats cardigan letterpress Carles pug XOXO church-key pork belly iPhone mixtape disrupt pickled. Single-origin coffee literally banh mi Pinterest, Neutra before they sold out Kickstarter.</p>
					</div>
					<div class="term-section">
						<div class="term-elements">
							<input class="check-input"  type="checkbox" name="selected" value="" id="confirm_final" />
							<div class="checkbox"  for="confirm"></div>
							<span>I, <?php echo $client['user_name']; ?>, confirm that by checking this digital signature, stumptown authentic, keytar. </span>
						</div>
					</div>


				</div>

			</form>

		</div>

	</div>

	<?php
		include 'footer.php';
	?>
</div>
</body>
</html>