<?php
	$curr_page = strtok($_SERVER["REQUEST_URI"],'?');
	$nav_items = array('home','send', 'receive', 'audit', 'admin', 'my account');
	$links = array();

	foreach ($nav_items as $nav_item) {
		$links[] = array(
			'text'	=> ucwords($nav_item),
			'file'  => '/' . str_replace(' ', '-', $nav_item) . '.php'
		);
	}
?>

<div id="topNav">
	<ul>

		<?php
			foreach($links as $link):
				echo '<li><a href="'.$link['file'].'"';

				if ($curr_page === $link['file']) {
					echo ' class="active"';
				}

				echo '>'.$link['text'].'</a></li>';
			endforeach;
		?>

		<li>
			<a href="?action=ulogout">Log Out</a>
		</li>
	</ul>
</div>