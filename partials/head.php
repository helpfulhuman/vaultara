	<meta charset="utf-8">
<!-- MAIN STYLE SHEETS -->
	<link rel="stylesheet" href="./css/main.css" media="screen">

	<!-- IE STYLESHEETS -->

	<!--[if IE 7]>
		<link href="css/IE/ie7.css" rel="stylesheet" media="screen" />
	<![endif]-->

	<!--[if IE 8]>
		<link href="css/IE/ie8.css" rel="stylesheet"  media="screen" />
	<![endif]-->

	<!--[if gt IE 8]>
		<link href="css/IE/ie9up.css" rel="stylesheet" media="screen" />
	<![endif]-->

	<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,400italic,700italic,300italic' rel='stylesheet' type='text/css'>

	<!-- JAVASCRIPT/JQUERY -->
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

	<!-- PlaceHolder JS -->

	<script src="./js/placeholders.jquery.min.js"></script>

	<!-- DatePicker -->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


  <!-- APPLICATION JS FUNCTIONS -->
	<script src="./js/app.js"></script>

	<!-- J-TABLE -->
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css">
  	<link href="./js/jtable/themes/metro/blue/jtable.min.css" rel="stylesheet" type="text/css" />