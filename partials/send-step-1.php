	<div class="send-container">
		<div class="send-header">
			<h2 class="header-text" id="send-head"><img id="send-icon" src="images/icon-send.png"></img>Send Images</h2>
		</div>
		<div class="send-subHeader">
			<span class="step-text">STEP</span>
			<div class="step-circle">1</div>
			<span class="send-filter-text">DICOM QUERY FILTER</span>
			<span class="instruction-text">Enter information into any ONE field below to filter results. Wildcards (*) can be used.</span>

			<div class="step2-item">
				<span class="step-text">STEP</span>
				<div class="step-circle">2</div>
			</div>
		</div>
		<div class="form-container">
			<form action="send.php?step=2" method="POST" id="form-send-id">
				<div class="cf">
					<p class="srcTag">Source:</p>
					<div class="styled-select">
						<select id="select-input">
	  					<option value="vpacs">VPACS</option>
	  					<option value="vpacs">PLACEHOLDER1</option>
	  					<option value="vpacs">PLACEHOLDER2</option>
						</select>
					</div>
					<div class="send-input-container">
						<input id="input-name" class="send-input" type="text"  placeholder="Patient Name">
						<input id="input-ID" class="send-input" type="text"  placeholder="Patient ID">
						<input id="datepicker" class="send-input" type="text" placeholder="Study Date (YYYY/MM/DD)">
						<input id="input-mod" class="send-input" type="text"   placeholder="Modality">
						<input id="input-accession" class="send-input" type="text" placeholder="Accession Number">
						<button id="send-button" name="form[send-button]" type="submit">SEARCH<img id="submit-img" src="images/icon-arrow.png"/></button>
					</div>
				</div>
			</form>
		</div>

	</div>