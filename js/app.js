$(function() {

  // $('input, textarea').placeholder();

  // Date-Picker Function
  $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  $( "#availability-field" ).datepicker({ dateFormat: 'yy-mm-dd' });


  // Custom Style Checkbox Support

  // $('input[type=checkbox], .checkbox').toggle()

  $('input[type=checkbox]').on('change', function () {
    var id = $(this).attr('id')
    // console.log("A CHANGE HAS OCCURED");
    if (id) {
      $labels = $('[for=' + id + ']')
      if (this.checked) {
        $labels.addClass('is-checked')
        console.log('IM CHECKED');

      }
      else {
        $labels.removeClass('is-checked')
        console.log('IM NOT CHECKED');


      }
    }
    // SECTION FOR SEND 2 HIDDEN BUTTON DIV
    //
    if($('input[type=checkbox]').is(':checked')){
      // if any of the checkboxes are checked then make the area visible
      // $('.button-form-wrapper').fadeIn(500);
      $('.button-form-wrapper').show();
    } else {
      // only if all are unchecked will this fire and hide the area
      // $('.button-form-wrapper').fadeOut(500);
      $('.button-form-wrapper').hide();
    }

  })

  $('.checkbox').on('click', function (e) {
    // e.preventDefault()
    // console.log(e);
    // console.log("GENERAL CHECKBOX HAS BEEN CLICKED");
    var tgtInput = '#' + $(this).attr('for')
    var $checkbox = $(tgtInput)
    var checked = $checkbox.prop('checked')
    $(this).toggleClass('is-checked')
    $checkbox.prop('checked', !checked)

    // console.log("CHANGE");

    $checkbox.trigger("change");
  })

  // For Send Step 2 Data Rows
  //
  //
  $('.data-row').on('click' , function(e) {
    // e.preventDefault()
    // console.log(e);
    // console.log("TABLE ROW CHECKBOX HAS BEEN CLICKED");
    var tgtInput = '#' + $(this).find(".checkboxRow").attr('for')
    // console.log("this is that target: ", tgtInput);
    var $checkbox = $(tgtInput)
    var checked = $checkbox.prop('checked')
    $(this).find(".checkbox").toggleClass('is-checked')
    $checkbox.prop('checked', !checked)

    console.log("CHANGE");

    $checkbox.trigger("change");
  })


  //TOGGLE SUBMIT BUTTON ON SEND_03

  $('#confirm_final').change(function(){

    if(this.checked){
      $('.submit-form').removeAttr("disabled");
      $('.submit-form').toggleClass("is_enabled")
    }
    else {
      $('.submit-form').toggleClass("is_enabled")
      $('.submit-form').attr("disabled", true);
    }
  });


  // For odd child row background color IE8
  //
  //
  $(".data-row:odd").css('background','#EBEBE7');
  // JS for hover in IE8
  $(".data-row:odd").hover(function (){
    $(this).css('background', '#CFEAE7')},
                   function (){
    $(this).css('background', '#EBEBE7');
  })



  // For Audit Div toggle
  //
  //
  $('[data-toggle]').on('click', function (e) {
    e.preventDefault()

    var target = $(this).data('toggle')
      , $target = $(target)

    $target.toggleClass('is-open')
    $(this).toggleClass('is-active')
    $(this).parent().toggleClass('is-active')
  })



  //Step 3 Permissions Checkboxes

  $('.perm1 input').on('change', function(){

        if(this.checked){
          var tgtInput = $('.perm2').find(".checkbox").attr('for')
          // console.log("this is that target: ", tgtInput);

          // uncheck viewer selection
          $("#viewer").attr('checked', false);
          $('[for="viewer"]').removeClass('is-checked')

          //uncheck image selection
          $("#images").attr('checked', false);
          $('[for="images"]').removeClass('is-checked')

        }
   });
  

    $('.perm2 input').on('change', function(){
        if (this.checked){
          $("#online").attr('checked', false);
          $('[for="online"]').removeClass('is-checked')
        }
    })

    // WHEN VIEWER IS CHECKED, IMAGE MUST ALSO BE CHECKED
    // WHEN VIEWER IS UNCHECKED, IMAGE SHOULD STAY CHECKED
    $('#viewer').on('change' , function(e) {
      // console.log("THIS IS E: ", e);
      // console.log(this);
      if(this.checked){
        $('#images').attr('checked', true);
        $('#viewer').attr('checked', true);
        $('[for="viewer"]').addClass('is-checked')
        $('[for="images"]').addClass('is-checked')
      } else {
        $('#viewer').attr('checked', false);
        $('[for="viewer"]').removeClass('is-checked')
      }
    })

    // WHEN IMAGE IS CHECKED, VIEWER CAN BE OFF.
    // WHEN IMAGE IS UNCHECKED, VIEWER MUST BE OFF.
    $('#images').on('change' , function(e) {
      // console.log("THIS IS E: ", e);
      // console.log(this);
      if(this.checked){
        $('#images').attr('checked', true);
        $('[for="images"]').addClass('is-checked')
      } else {
        $('#images').attr('checked', false);
        $('#viewer').attr('checked', false);
        $('[for="images"]').removeClass('is-checked')
        $('[for="viewer"]').removeClass('is-checked')
      }
    })

  

});
