<!DOCTYPE html>
<html>
<head>
	<title>Send</title>
	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="css/style.css" media="screen">
</head>
<body>
<div id="container">
	<?php
		include 'partials/navbar.php';
	?>
	
	<div id="companyBanner">
		<p id="vaultaraLogo">Send
		</p>
	</div>

	<?php
		include 'sendMainNav.php';
	?>

	<?php
		include 'footer.php';
	?>
</div>
</body>
</html>
