<!DOCTYPE HTML>
<html class="home-background">
  <head>
  <?php
    include 'partials/head.php';
  ?>
  </head>
  <body>
    <div id="container">
      <?php
        include 'partials/navbar.php';
      ?>

      <div id="companyBanner" class="admin-user-banner">

        <img id="admin-user-logo-image" src="images/glowgo.png">
        <!-- <p id="vaultaraLogo">vaultara
        </p> -->
      </div>
      <div class="admin-user">
        <div class="admin-user-container">
          <div class="admin-user-header">
            <div class="header-text" id="admin-head">
              <img id="admin-user-icon" src="images/gears.png"></img>
              <div class="admin-user-header-text">Admin: User Management</div>
            </div>
          </div>
          <div class="admin-user-body">
            <div class="admin-user-manage">
              <a href="#" id="manage-option">Manage user permissons</a>
              <span>|</span>
              <a href="#" id="manage-option"> Manage recipients </a>
            </div>
            <div id="PersonTableContainer"></div>
          </div>
        </div>
      </div>
      <?php
    include 'footer.php';
    ?>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
    <script src="js/jtable/jquery.jtable.min.js" type="text/javascript"></script>

    <script type="text/javascript">
      $(document).ready(function () {
        $('#PersonTableContainer').jtable({
          title: 'Users',
          actions: {
            listAction: 'js/jtable/PersonList.php',
            createAction: 'js/jtable/CreatePerson.php',
            updateAction: 'js/jtable/UpdatePerson.php',
            deleteAction: 'js/jtable/DeletePerson.php'
          },
          fields: {
            First: {
              title: 'First',
              width: '26%',
            },
            Last: {
              title: 'Last',
              width: '26%'
            },
            Email: {
              title: 'Email',
              width: '26%'
            }
          }
        });
      });

      // $('#PersonTableContainer').jtable('load');
    </script>

  </body>

</html>