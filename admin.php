<!DOCTYPE HTML>
<html class="send-background">
  <head>

    <?php
      include 'partials/head.php';
    ?>
  </head>
  <body>
    <div id="container">
      <?php
        include 'partials/navbar.php';
      ?>

      <div class="banner-background">
        <div id="companyBanner" class="send-banner">

          <img id="send-logo-image" src="images/glowgo.png">

        </div>
      </div>
      <div class="admin">
        <div class="admin-container">
          <div class="admin-header">
            <div class="header-text" id="admin-head">
              <img id="admin-icon" src="images/gears.png"></img>
              <div class="admin-header-text">Admin</div>
            </div>
          </div>

          <div class="admin-body">
            <div class="choices">
              <div class="choice">
                <div class="choicelink">
                  <a href="#">Core Configuration Settings</a>
                </div>
              </div>

              <div class="choice">
                <div class="choicelink">
                  <a href="#">User Administration/Permissions</a>
                </div>
              </div>

              <div class="choice">
                <div class="choicelink">
                  <a href="#">Recipient Management</a>
                </div>
              </div>

              <div class="choice">
                <div class="choicelink">
                  <a href="#">Variable Management</a>
                </div>
              </div>

              <div class="choice">
                <div class="choicelink">
                  <a href="#">Control Management</a>
                </div>
              </div>

              <div class="choice">
                <div class="choicelink">
                  <a href="#">DICOM Management</a>
                </div>
              </div>

              <div class="choice">
                <div class="choicelink">
                  <a href="#">File Extensions Permissions</a>
                </div>
              </div>

            </div>

          </div>
        </div>
      </div>

    <?php
      include 'footer.php';
    ?>

    </div>
  </body>
</html>



